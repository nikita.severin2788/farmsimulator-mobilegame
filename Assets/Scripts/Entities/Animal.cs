﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.Events;

public class Animal:MonoBehaviour, IConfig {

    [SerializeField] public string Type;


    [Header("ProgressBar")]
    [SerializeField] private GameObject ProgressBar;
    [SerializeField] private GameObject SetProgressBar;
    [SerializeField] private Transform ProgressBarSpawnPoint;

    [Header("ResourceImage")]
    [SerializeField] private Transform ResurceSpanwPoint;
    [SerializeField] private GameObject ResurceObject;
    [SerializeField] private GameObject SetResurceObject;

    [Header("Entity")]
    public EntityConfigurationData Entity;

    [SerializeField] public Cell CellComponent { get; set; }

    [SerializeField] public CellDataConfig CellData { get; set; } = new CellDataConfig();


    

    private void Start() {

        InitializeData();
        SpawnProgressBar();
        ProgressBarUpdate();

        if(CellData.isDone)
            SpawnResourceIcon();

        StartCoroutine(AllUpdate());
    }

    public void InitializeData() {
        Entity = ItemsManager.Instance.GetData(Type);
        CellData.EntityType = Type;

        if(CellComponent.LoadData().EntityType != Type)
            Save();
        else
            CellData = CellComponent.LoadData();
    }

    public void EventClick() {
        DestroyResourceIcon();
        AddEat();
        AddResource();
    }

    IEnumerator AllUpdate() {

        while(true) {

            EntityUpdate();    
            ProgressBarUpdate();
            Save();

            yield return new WaitForSeconds(1f);
        }

    }

    private void EntityUpdate() {
        if(CellData.ResourceCount > 0 && !CellData.isDone) {
            if(CellData.Time < Entity.TimeToGenerateResource) {
                CellData.Time++;
            } else {
                CellData.ResourceCount--;
                CellData.Time = 0;
                CellData.isDone = true;
                SpawnResourceIcon();
            }
        }
    }

    private void SpawnProgressBar() {
        SetProgressBar = Instantiate(ProgressBar,ProgressBarSpawnPoint.position,ProgressBarSpawnPoint.rotation);
        SetProgressBar.transform.SetParent(transform);
    }

    private void ProgressBarUpdate() {
        if(SetProgressBar != null) {
            if(CellData.ResourceCount < 1) {
                SetProgressBar.SetActive(false);
            } else {
                SetProgressBar.SetActive(true);
                SetProgressBar.GetComponent<ProgressBarAnimal>().BarUpdate(CellData.Time,Entity.TimeToGenerateResource);
                SetProgressBar.GetComponent<ProgressBarAnimal>().ResourceCountBar(CellData.ResourceCount);
            }
        }
    }

    private void SpawnResourceIcon() {
        SetResurceObject = Instantiate(ResurceObject,ResurceSpanwPoint.position,ResurceSpanwPoint.rotation);
        SetResurceObject.transform.SetParent(transform);
    }

    private void DestroyResourceIcon() {
        if(CellData.isDone)
        Destroy(SetResurceObject);
    }

    private void AddResource() {
        if(CellData.isDone) {
            ResourcesManager.Instance.AddResource(Entity.Resource,1);
            CellData.isDone = false;
        }
    }

    private void AddEat() {
        if(CellData.ResourceCount < 1) {
            if(ResourcesManager.Instance.GetResourceCount(Entity.EatType) > 0) {
                ResourcesManager.Instance.AddResource(Entity.EatType,-1);
                CellData.ResourceCount = (int)(Entity.TimeInOneEat / Entity.TimeToGenerateResource);
            }
        }
    }
    private void Save() {
        CellComponent.SaveData(CellData);
    }

    
}
