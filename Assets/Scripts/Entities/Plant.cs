﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : MonoBehaviour , IConfig
{
    [SerializeField] public string Type;

    [SerializeField] public CellDataConfig CellData { get; set; } = new CellDataConfig();

    [SerializeField] public Cell CellComponent { get; set; }

    public EntityConfigurationData Entity;

    [SerializeField] public bool isGrowDone;

    [Header("Sprits")]
    [SerializeField] private SpriteRenderer Sprite;
    [SerializeField] private Sprite GrowImage;
    [SerializeField] private Sprite GrowDoneImgae;




    private void Start() {

        InitializeData();

        Grow();
        StartCoroutine(AnimalUpdate());
    }

    public void InitializeData() {
        Entity = ItemsManager.Instance.GetData(Type);
        CellData.EntityType = Type;

        if(CellComponent.LoadData().EntityType != Type)
            Save();
        else
            CellData = CellComponent.LoadData();
    }

    IEnumerator AnimalUpdate() {
        while(true) {
            if(!CellData.isDone) {
                if(CellData.Time <= Entity.TimeToGenerateResource) {
                    CellData.Time++;
                } else {
                    CellData.isDone = true;
                    Grow();
                    Save();
                }
            }

            
            yield return new WaitForSeconds(1f);
        }

        
    }

    private void Grow() {
        if(CellData.isDone) {
            Sprite.sprite = GrowDoneImgae;
        } else {
            Sprite.sprite = GrowImage;
        }
    }
    

    public void Collect() {
        if(CellData.isDone) {
            CellData.Time = 0;
            AddResource();
            CellData.isDone = false;
            Grow();
        }
    }

    public void Save() {
        CellComponent.SaveData(CellData);
    }

    private void AddResource() {
        ResourcesManager.Instance.AddResource(Entity.Resource,1);
    }
}

