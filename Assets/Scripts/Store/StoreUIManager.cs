﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StoreUIManager : MonoBehaviour
{

    public GameObject SellPanel;
    public GameObject BuyPanel;
    public GameObject StorePanel;

    public static event UnityAction<bool> isShowStore;

    private bool isSpawnMode;

    public void OnEnable() {
        ItemsManager.SpawnMode += EditMode;
        RemoveEntityManger.RemoveEvent += EditMode;
    }

    public void OnDisable() {
        ItemsManager.SpawnMode -= EditMode;
        RemoveEntityManger.RemoveEvent -= EditMode;
    }

    public void EditMode(bool state) {
        if(state)
            HideStore();

        isSpawnMode = state;

    }

    public void ShowBuyPanel() {
        SellPanel.SetActive(false);
        BuyPanel.SetActive(true);
    }

    public void ShowCellPannel() {
        SellPanel.SetActive(true);
        BuyPanel.SetActive(false);
    }

    public void ShowStore() {
        if(isSpawnMode == false) {
            StorePanel.SetActive(true);
            isShowStore?.Invoke(true);
        }
    }

    public void HideStore() {
        StorePanel.SetActive(false);
        isShowStore?.Invoke(false);
    }
}
