﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SellResource : MonoBehaviour
{

    [SerializeField] private string ResourceName;
    [SerializeField] private Text TitleText;
    [SerializeField] private Text PriceText;
    [SerializeField] private Text ResourceCount;
    [SerializeField] private Image Icon;
    private ResourceItem setResource;

    public void Start() {
        GetInfo();
    }

    private void OnEnable() {
        ResourcesManager.UpdateUI += GetInfo;
        GetInfo();
    }

    private void OnDisable() {
        ResourcesManager.UpdateUI -= GetInfo;
    }

    public void GetInfo() {
        if(ResourcesManager.Instance.GetResource(ResourceName) != null) {

            setResource = ResourcesManager.Instance.GetResource(ResourceName);
            TitleText.text = setResource.Name;
            PriceText.text = setResource.Price.ToString();
            ResourceCount.text = "Count: " + setResource.Count.ToString();
            Icon.sprite = setResource.Image;
        }
    }

    public void Sell() {

        if(ResourcesManager.Instance.GetResourceCount(ResourceName) > 0) {
            ResourcesManager.Instance.AddResource(ResourceName,-1);
            ResourcesManager.Instance.AddResource("Coin",setResource.Price);
            
        }
    }
}
