﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyResource : MonoBehaviour
{

    [SerializeField] private string EntityName;
    [SerializeField] private Text TitleText;
    [SerializeField] private Text PriceText;
    [SerializeField] private Image Icon;
    private EntityConfigurationData EntityData;

    public void Start() {
        GetInfo();
    }


    public void GetInfo() {
        if(ItemsManager.Instance.GetData(EntityName) != null) {
            EntityData = ItemsManager.Instance.GetData(EntityName);
            TitleText.text = EntityData.Name;
            PriceText.text = EntityData.Price.ToString();
            Icon.sprite = EntityData.Image;
        }
    }

    public void Buy() {

        if(ResourcesManager.Instance.GetResourceCount("Coin") >= EntityData.Price) {
            ResourcesManager.Instance.AddResource("Coin",- EntityData.Price);
            ItemsManager.Instance.SpanwModeBegin(EntityName);
        }
    }
}
