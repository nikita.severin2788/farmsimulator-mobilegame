﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveEntity : MonoBehaviour
{
    private bool removeState;

    private void OnEnable() {
        RemoveEntityManger.RemoveEvent += RemoveState;
    }
    private void OnDisable() {
        RemoveEntityManger.RemoveEvent -= RemoveState;
    }

    public void RemoveState(bool state) {
        removeState = state;
    }

    public void EventClick() {
        if(removeState)
            DestroyEntity();
    }

    public void DestroyEntity() {
        if(GetComponent<IConfig>() != null)
        GetComponent<IConfig>().CellComponent.Remove();

        Destroy(gameObject);
    }
}
