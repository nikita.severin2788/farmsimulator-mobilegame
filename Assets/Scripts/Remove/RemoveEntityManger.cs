﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RemoveEntityManger : MonoBehaviour
{

    public static event UnityAction<bool> RemoveEvent;

    public GameObject Button;

    private bool RemoveState;

    private void OnEnable() {
        ItemsManager.SpawnMode += DisableButton;
    }

    private void OnDisable() {
        ItemsManager.SpawnMode -= DisableButton;
    }

    public void DisableButton(bool state) {
        Button.SetActive(!state);
    }

    public void Remove() {
        
        RemoveState = !RemoveState;
        RemoveEvent?.Invoke(RemoveState);
    }
}
