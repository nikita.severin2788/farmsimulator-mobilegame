﻿using UnityEngine;

[System.Serializable]
public class EntityConfigurationData
{
    public string Name;
    public string Resource;
    public string EatType;
    public int TimeInOneEat;
    public int Price;
    public int TimeToGenerateResource;
    public Sprite Image;
    public GameObject Prefab;
    

    public int Eat;

    public EntityConfigurationData(string name,string resource,string eatType,int timeInOneEat, int price, int timeToGenerateResource, Sprite image,  GameObject prefab) {
        Name = name;
        Resource = resource;
        EatType = eatType;
        TimeInOneEat = timeInOneEat;
        Price = price;
        TimeToGenerateResource = timeToGenerateResource;
        Image = image;
        Prefab = prefab;
    }
}