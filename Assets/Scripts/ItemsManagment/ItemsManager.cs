﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using System.IO;

public class ItemsManager : MonoBehaviour
{
    public static ItemsManager Instance;

    public static event UnityAction<bool> SpawnMode; 

    public List<EntityConfigurationData> ConfigDataList  = new List<EntityConfigurationData>();

    public string SelectItem;

    private void Awake() {
        Instance = this;
    }

    public void Start() {
        LoadData();
    }

    public void SpanwModeBegin(string EntityType) {
        SelectItem = EntityType;
        SpawnMode?.Invoke(true);
    }

    public void SpawnModeEnd() {
        SpawnMode?.Invoke(false);
    }

    public EntityConfigurationData GetData(string name) {
        foreach(EntityConfigurationData SetData in ConfigDataList) {
            if(SetData.Name == name) {
                return SetData;
            }
        }
        return null;
    }

    public void LoadData() {
        for(int i = 0; i < ConfigDataList.Count; i++) {

            TextAsset json = Resources.Load<TextAsset>("Configurations/EntityConfigs/" + ConfigDataList[i].Name + "Configurations");
            EntityConfigurationData SetConfigData = JsonUtility.FromJson<EntityConfigurationData>(json.text);

            ConfigDataList[i].Name = SetConfigData.Name;
            ConfigDataList[i].Resource = SetConfigData.Resource;
            ConfigDataList[i].EatType = SetConfigData.EatType;
            ConfigDataList[i].TimeInOneEat = SetConfigData.TimeInOneEat;
            ConfigDataList[i].TimeToGenerateResource = SetConfigData.TimeToGenerateResource;
            ConfigDataList[i].Price = SetConfigData.Price;
        }
    }

}
