﻿using UnityEngine;
using UnityEngine.Events;

public class EventClick:MonoBehaviour {
    public UnityEvent Event;
    public bool isShowStore;

    private void OnEnable() {
        StoreUIManager.isShowStore += StoreShow;
    }

    private void OnDisable() {
        StoreUIManager.isShowStore += StoreShow;
    }

    public void StoreShow(bool state) {
        isShowStore = state;
    }

    public void OnMouseDown() {
        if(!isShowStore)
            Event.Invoke();
    }
}
