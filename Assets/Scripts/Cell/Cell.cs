﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class Cell : MonoBehaviour
{

    public int id;
    
    public bool isEmpty = true;
    public bool isSpawn = false;


    private GameObject EntityObject;
    public Transform SpawnPoint;

    [Header("IndiacateColors")]
    public SpriteRenderer Renderer;
    public Color DefaultColor;
    public Color CellIsEmptyColor;
    public Color CellIsNotEmptyColor;

    [Header("CellData")]
    public CellDataConfig CellConfig = new CellDataConfig();



    private void Start() {
        if(LoadData() == null) {
            Debug.Log("DataLoaded");
            SaveData(new CellDataConfig());
        }
        InitializeData();
    }

    public void OnEnable() {
        ItemsManager.SpawnMode += CellIndicate;
        ItemsManager.SpawnMode += SpawnMode;
        RemoveEntityManger.RemoveEvent += CellIndicate;
    }

    public void OnDisable() {
        ItemsManager.SpawnMode -= CellIndicate;
        ItemsManager.SpawnMode -= SpawnMode;
        RemoveEntityManger.RemoveEvent += CellIndicate;
    }


    

    public void Spawn() {

        if(isEmpty && isSpawn) {

            if(ItemsManager.Instance.GetData(ItemsManager.Instance.SelectItem) != null) {

                EntityConfigurationData EntityData = ItemsManager.Instance.GetData(ItemsManager.Instance.SelectItem);

                EntityObject = Instantiate(EntityData.Prefab,SpawnPoint.position,Quaternion.Euler(0,0,0));
                EntityObject.transform.SetParent(SpawnPoint);
                EntityObject.GetComponent<IConfig>().CellComponent =this;

                isEmpty = false;

                transform.GetComponent<Collider2D>().enabled = false;

                ItemsManager.Instance.SpawnModeEnd();
            }
        }
    }

    public void SpawnMode(bool state) {
        isSpawn = state;
    }

    public void Remove() {
        isEmpty = true;
        SaveData(new CellDataConfig());
        CellIndicate(true);
    }

    public void CellIndicate(bool state) {

        if(state) {
            if(isEmpty)
                Renderer.color = CellIsEmptyColor;
            else
                Renderer.color = CellIsNotEmptyColor;
        } else {
            Renderer.color = DefaultColor;
        }
    }

    public void SaveData(CellDataConfig config) {

        string json = JsonUtility.ToJson(config);
        File.WriteAllText( Application.persistentDataPath + "/Cell_" + id.ToString() + ".json",json);

    }

    public CellDataConfig LoadData() {

        if(File.Exists( Application.persistentDataPath + "/Cell_" + id.ToString() + ".json")) {
            string json = File.ReadAllText(Application.persistentDataPath + "/Cell_" + id.ToString() + ".json");
            return JsonUtility.FromJson<CellDataConfig>(json);
        } else {
            return null;
        }
    }

    public void InitializeData() {

        CellDataConfig SetData = LoadData();

        if(ItemsManager.Instance.GetData(SetData.EntityType) != null) {

            EntityConfigurationData EntityData = ItemsManager.Instance.GetData(SetData.EntityType);

            EntityObject = Instantiate(EntityData.Prefab,SpawnPoint.position,Quaternion.Euler(0,0,0));
            EntityObject.transform.SetParent(SpawnPoint);
            EntityObject.GetComponent<IConfig>().CellComponent = this;

            transform.GetComponent<Collider2D>().enabled = false;
            isEmpty = false;
        }
    }
}

