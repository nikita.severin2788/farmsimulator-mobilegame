﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgressBarAnimal : MonoBehaviour
{

    public float LineLengtch;
    public float LineDefaultLengtch;
    public int ResourceGenerateCount;
    public Transform Line;
    public TextMesh Text;


    public void BarUpdate(int time,float timeToenerateResources) {

        if(timeToenerateResources >= time) {
            float OneStepLengtch = LineDefaultLengtch / timeToenerateResources;

            LineLengtch = time * OneStepLengtch;

            Line.localScale = new Vector3(LineLengtch,Line.localScale.y,Line.localScale.z);
        } else {
            Line.localScale = new Vector3(0f,Line.localScale.y,Line.localScale.z);
        }
    }

    public void ResourceCountBar(int count) {
        Text.text = count.ToString();
    }



}
