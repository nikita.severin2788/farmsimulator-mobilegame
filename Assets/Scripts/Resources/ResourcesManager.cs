﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System.IO;

public class ResourcesManager:MonoBehaviour {

    public static ResourcesManager Instance;

    public List<ResourceItem> ResourceList = new List<ResourceItem>();

    public static event UnityAction UpdateUI;

    private void Awake() {
        Instance = this;
    }

    private void Start() {

        LoadResources();
        SaveResources();
        UpdateUI?.Invoke();
        
    }

    public void AddResource(string name, int count) {
        foreach(ResourceItem SetResource in ResourceList) {
            if(SetResource.Name == name) {
                SetResource.Count += count;
            }
        }
        UpdateUI?.Invoke();
        SaveResources();
    }

    public int GetResourceCount(string name) {

        for(int i = 0; i < ResourceList.Count; i++) {
            if(ResourceList[i].Name == name) {
                return ResourceList[i].Count;
            }
        }
        return 0;
    }
    public ResourceItem GetResource(string name) {
        for(int i = 0; i < ResourceList.Count; i++) {
            if(ResourceList[i].Name == name) {
                return ResourceList[i];
            }
        }
        return null;
    }

    public void SaveResources() {
        for(int i = 0; i < ResourceList.Count; i++) {
            string json = JsonUtility.ToJson(ResourceList[i]);
            File.WriteAllText(Application.persistentDataPath + "/" + ResourceList[i].Name + ".json",json);
        }   
    }

    public void LoadResources() {
        for(int i = 0; i < ResourceList.Count; i++) {
            TextAsset json = (TextAsset)Resources.Load("Configurations/ResourcesConfigs/" + ResourceList[i].Name, typeof(TextAsset));
            ResourceItem SetRes = JsonUtility.FromJson<ResourceItem>(json.text);
            ResourceList[i].Name = SetRes.Name;
            ResourceList[i].Price = SetRes.Price;

            if(File.Exists(Application.persistentDataPath + "/" + ResourceList[i].Name + ".json")) {
                string stringJson = File.ReadAllText(Application.persistentDataPath + "/" + ResourceList[i].Name + ".json");
                ResourceList[i].Count = JsonUtility.FromJson<ResourceItem>(stringJson).Count;
            }else {
                SaveResources();
                AddResource("Coin",100);
            }
        }
    }

}
[System.Serializable]
public class ResourceItem {

    public string Name;
    public int Count;
    public int Price;
    public Sprite Image;

    public ResourceItem(string name,int count,int price,Sprite image) {
        Name = name;
        Count = count;
        Price = price;
        Image = image;
    }
}
