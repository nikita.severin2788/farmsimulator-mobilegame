﻿using UnityEngine;
using UnityEngine.UI;

public class ResourceUI : MonoBehaviour
{

    [SerializeField] private Text TextField;
    [SerializeField] private string ResoursceName;

    private void Start() {
        TextUpdate();
    }

    private void OnEnable() {
        ResourcesManager.UpdateUI += TextUpdate;
    }

    private void OnDisable() {
        ResourcesManager.UpdateUI -= TextUpdate;
    }

    private void TextUpdate() {
        TextField.text= ResourcesManager.Instance.GetResourceCount(ResoursceName).ToString();
    }
}
