﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


interface IConfig {
    CellDataConfig CellData { get; set; }
    Cell CellComponent { get; set; }
}

[System.Serializable]
public class CellDataConfig {

    public string EntityType;
    public int Time;
    public int ResourceCount;
    public bool isDone;

}
